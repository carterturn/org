package org.nuevaschool.la;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import org.opencv.highgui.Highgui;
import javax.swing.ImageIcon;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.Image;

public class ImgIO extends Highgui{
	/**
	   This function from http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
	*/
	public static BufferedImage MatToBufferedImage(Mat mat){

		int type = BufferedImage.TYPE_BYTE_GRAY;
		if ( mat.channels() > 1 ) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = mat.channels()*mat.cols()*mat.rows();
		byte [] b = new byte[bufferSize];
		mat.get(0,0,b); // get all the pixels
		BufferedImage image = new BufferedImage(mat.cols(), mat.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;

	}

	/**
	   This function from http://www.answers.opencv.org/question/8119/how-to-display-image-on-java-release/
	*/
	public static void displayImage(Image img){
		ImageIcon icon=new ImageIcon(img);
		JFrame frame=new JFrame();
		frame.setLayout(new FlowLayout());
		frame.setSize(img.getWidth(null)+50, img.getHeight(null)+50);
		JLabel lbl=new JLabel();
		lbl.setIcon(icon);
		frame.add(lbl);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	public static void imshow(Mat img){
		displayImage(MatToBufferedImage(img));
	}
}
